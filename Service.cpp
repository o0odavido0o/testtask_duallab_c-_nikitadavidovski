#include "service.h"

bool Service::IsMoreEffective(const Service& service) const {

	if (this->departure == service.departure &&
		this->arrival < service.arrival) {
		return true;
	}
	if (this->departure > service.departure &&
		this->arrival == service.arrival) {
		return true;
	}
	if (this->departure > service.departure &&
		this->arrival < service.arrival) {
		return true;
	}
	if (this->departure == service.departure &&
		this->arrival == service.arrival &&
		this->company == "Posh" && service.company == "Grotty") {
		return true;
	}

	return false;
}

std::ostream& operator<<(std::ostream& out, const Service& service) {
	out << service.company << " " << service.departure << " " << service.arrival;
	return out;
}