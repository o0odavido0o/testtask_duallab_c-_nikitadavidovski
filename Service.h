#pragma once
#include "time.h"

class Service {

private:

	std::string company;
	Time departure;
	Time arrival;

public:

	Service(const std::string& company, const int& departureHours, const int& departureMins, const int& arrivalHours, const int& arrivalMins) {
		this->company = company;
		departure = Time(departureHours, departureMins);
		arrival = Time(arrivalHours, arrivalMins);
	}

	const std::string& GetCompany() const {
		return company;
	}

	const Time& GetDepartureTime() const {
		return departure;
	}

	const Time& GetArrivalTime() const {
		return arrival;
	}

	friend std::ostream& operator<<(std::ostream& out, const Service& s);

	bool IsMoreEffective(const Service& service) const;
};