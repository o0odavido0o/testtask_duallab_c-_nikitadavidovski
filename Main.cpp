#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include "service.h"

std::vector<Service> GetServicesWithTheRequiredDurationFromFile(const std::string& filename);
bool ServiceIsShorterThanHour(const int& departureHours, const int& departureMins, const int& arrivalHours, const int& arrivalMins);

void SortServicesByDepartureTime(std::vector<Service>& services);

std::vector<Service> ChooseEffectiveServices(const std::vector<Service>& services);

void PrintTimeTables(const std::string& filename, const std::vector<Service>& services);

int main() {

	std::vector<Service> services;
	std::vector<Service> effectiveServices;

	services = GetServicesWithTheRequiredDurationFromFile("input.txt");
	SortServicesByDepartureTime(services);
	effectiveServices = ChooseEffectiveServices(services);
	PrintTimeTables("output.txt", effectiveServices);

	return 0;
}

std::vector<Service> GetServicesWithTheRequiredDurationFromFile(const std::string& filename) {

	std::ifstream fin(filename);
	std::vector<Service> services;
	std::string companyName;
	std::string departureTime;
	std::string arrivalTime;
	int departureHours;
	int departureMins;
	int arrivalHours;
	int arrivalMins;

	while (fin >> companyName) {
		fin >> departureTime;
		fin >> arrivalTime;

		departureHours = stoi(departureTime.substr(0, 2));
		departureMins = stoi(departureTime.substr(3, 5));
		arrivalHours = stoi(arrivalTime.substr(0, 2));
		arrivalMins = stoi(arrivalTime.substr(3, 5));
		if (ServiceIsShorterThanHour(departureHours, departureMins, arrivalHours, arrivalMins)) {
			services.push_back(Service(companyName, departureHours, departureMins, arrivalHours, arrivalMins));
		}
	}

	return services;
}

bool ServiceIsShorterThanHour(const int& departureHours, const int& departureMins, const int& arrivalHours, const int& arrivalMins) {

	int durationOfServiceInMins = (departureHours - arrivalHours) * 60 + departureMins - arrivalMins;
	if (durationOfServiceInMins > 60)
		return false;

	return true;
}

void SortServicesByDepartureTime(std::vector<Service>& services) {

	std::sort(services.begin(), services.end(),
		[](const Service& service1, const Service& service2) -> bool {
			return service1.GetDepartureTime() < service2.GetDepartureTime();
		});
}

std::vector<Service> ChooseEffectiveServices(const std::vector<Service>& services) {

	std::vector<Service> effectiveServices;

	for (int i = 0; i < services.size(); i++) {

		bool isMoreEffective = true;
		for (int j = 0; j < services.size(); j++) {

			if (j != i && services[j].IsMoreEffective(services[i])) {
				isMoreEffective = false;
				break;
			}
		}

		if (isMoreEffective) {
			effectiveServices.push_back(services[i]);
		}
	}

	return effectiveServices;
}

void PrintTimeTables(const std::string& filename, const std::vector<Service>& services) {

	std::ofstream fout(filename);

	for (auto service : services)
		if (service.GetCompany() == "Posh")
			fout << service << std::endl;

	fout << std::endl;

	for (auto service : services)
		if (service.GetCompany() == "Grotty")
			fout << service << std::endl;
}