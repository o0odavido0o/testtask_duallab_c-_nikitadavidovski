#include "time.h"

std::ostream& operator<<(std::ostream& out, const Time& time) {

	if (time.hours < 10) {
		out << "0";
	}

	out << time.hours << ":";

	if (time.minutes < 10) {
		out << "0";
	}

	out << time.minutes;

	return out;
}