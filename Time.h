#pragma once
#include <iostream>

class Time {

private:

	int hours;
	int minutes;
	// diffrence between hours:minutes and 00:00 in minutes
	int toMinutes;

public:

	Time() : hours(), minutes(), toMinutes() {};

	Time(const int& hours, const int& minutes) : hours(hours), minutes(minutes), toMinutes(hours * 60 + minutes){};

	bool operator<(const Time& time) const {
		return this->toMinutes < time.toMinutes;
	}

	bool operator==(const Time& time) const {
		return this->toMinutes == time.toMinutes;
	}

	bool operator>(const Time& time) const {
		return this->toMinutes > time.toMinutes;
	}

	friend std::ostream& operator<<(std::ostream& out, const Time& time);
};